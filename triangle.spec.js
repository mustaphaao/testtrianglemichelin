const expect = require('expect');
const triangle = require('./triangle.js');

describe('triangle', () => {

   it('should work with 1', () => {
      // arrange
      // NOTE: We dont need to arrange anything here
      // act
      const result = triangle.triangle(1);
      //asssert
      expect(result).toBe('A');
   });

   it('should work with 3', () => {
      // arrange
      // NOTE: We dont need to arrange anything here
      // act
      const result = triangle.triangle(3);
      //asssert
      expect(result).toBe('__A__\n_AAA_\nAAAAA');
   });

   it('should work with 0', () => {
      // arrange
      // NOTE: We dont need to arrange anything here
      // act
      const result = triangle.triangle(0);
      //asssert
      expect(result).toBe('');
   });

   it('should work with -1', () => {
      // arrange
      // NOTE: We dont need to arrange anything here
      // act
      const result = triangle.triangle(0);
      //asssert
      expect(result).toBe('');
   });

});