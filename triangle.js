


function triangleInit(n) {
   const ret = [];
   let line = [];
   let jump = 0;

   for (let i = 0; i < n; i++) {
      line = [];
      for (let j = 0; j < n; j++) {
         if (j <= jump)
            line.push('A');
         else
            line.push('_');
      }
      jump++;
      ret.push(line);
   }
   return ret;
}

function triangle(n) {
   let ret = []

   for (let l of triangleInit(n)) {
      const line = l.slice(); // dans le but de dupliquer et non de copier l'adresse mémoire
      ret.push(
         line.reverse().slice(0, -1).join('')
         + l.join('')
      );
   }
   return ret.join('\n');
}

// console.log(triangle(3));

module.exports = {
   triangle
}